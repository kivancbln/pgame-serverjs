import { Controller, Get, Post } from '@nestjs/common';
import { AppService } from './app.service';
import { TwitterApiService } from './twitter-api/twitter-api.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService, 
              private readonly twitterService: TwitterApiService) {
    
  }
  

  @Get()
  getHello(): string {
    return this.appService.getHello();
  }

  @Get('tweets')
  async findAll() {
    console.log('kivanc1');
    let res =  await this.twitterService.findAll().toPromise();
    return res.data;
  }

  @Post()
  async follow(){
    let res = await this.twitterService.followUserByUserName('pierrecartonpie').toPromise();
    return res.data;
  }  
  
}
