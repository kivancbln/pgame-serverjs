import { Module, HttpModule } from '@nestjs/common';
import { OddsService } from './odds.service';


@Module({
    imports: [HttpModule],
  providers: [OddsService],
  exports: [OddsService]
})
export class OddsModule {}