export interface MatchOdd {
    fixture_id: number,
    odd_desc: string,
    odd_value: string,
    rate: number

}
