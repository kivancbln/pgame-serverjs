import { Injectable, HttpService, Logger } from '@nestjs/common';
import { Observable } from 'rxjs';
import Axios, { AxiosResponse } from 'axios';
import { Cron } from '@nestjs/schedule';
import { KeyPairKeyObjectResult } from 'crypto';
import OAuth = require('oauth-1.0a')
const crypto = require('crypto')

@Injectable()
export class TwitterApiService {
    private readonly logger = new Logger(TwitterApiService.name);
    token: string;
    useraccesstoken: string;
    consumerKey: string;
    consumerSecret: string;
    useraccesssecret: string;
    constructor(private httpService: HttpService) {
        this.token = 'AAAAAAAAAAAAAAAAAAAAACCgGgEAAAAAReTCHDAGtojNrQuKDfu5hDpFa%2FA%3D109GvIpUirKJ8VvFHRBH8sKjdlN7ApzrkJ4kVNRqB8fCFmY40v';
        this.consumerKey = 'NCdO54Bwaq5eaXu5YZLZXloNa';
        this.consumerSecret = 'E9gmLM6okzEKfOOFHBtJSkS5lWlvKrQZ1YaKfs1Tn93Ikg9FnO';
        this.useraccesstoken = '1271738895290466304-30nfrhfzjQygcQUkc8hUHi2OflYsQm';
        this.useraccesssecret = 'RpW8gOcn4K5qcg9vqoyioMiV2k4xJ87XQ8eJKi6C4pTKw';

        Axios.defaults.headers.common = { 'Authorization': `Bearer ${this.token}` };
        
    }
    findAll(): Observable<AxiosResponse<any>> {
        let test = this.httpService.get('https://api.twitter.com/1.1/search/tweets.json?q=fenerbahce&src=typed_query&f=live');
        return test;
    }

    getUserByUserName(userName: string): Observable<AxiosResponse<any>> {
        return this.httpService.get(`https://api.twitter.com/1.1/users/show.json?screen_name=${userName}`);
    }

    getTweetsByUserName(userName: string): Observable<AxiosResponse<any>> {
        return this.httpService.get(`https://api.twitter.com/1.1/statuses/user_timeline.json?screen_name=${userName}`)
    }

    followUserByUserName(userName: string): Observable<AxiosResponse<any>> {
        const oauth = new OAuth({
            consumer: {
                key: this.consumerKey,
                secret: this.consumerSecret
            },
            signature_method: 'HMAC-SHA1',
            hash_function(base_string, key) {
                return crypto
                    .createHmac('sha1', key)
                    .update(base_string)
                    .digest('base64')
            },
        })

        const request_data = {
            url: `https://api.twitter.com/1.1/friendships/create.json?screen_name=${userName}&follow=true`,
            method: 'POST',
            data: null
        }

        const token = {
            key: this.useraccesstoken,
            secret: this.useraccesssecret
        }
        const hdrs = oauth.toHeader(oauth.authorize(request_data, token));
        this.logger.debug(hdrs);
        return this.httpService.post(`https://api.twitter.com/1.1/friendships/create.json?screen_name=${userName}&follow=true`, null, { headers: hdrs });

    }

    //@Cron('45 * * * * *')
    @Cron('* * 45 * * *')
    async handleCron() {
        this.logger.debug('entered cron');
        const tweets = (await this.findAll().toPromise()).data.statuses;
        // this.logger.debug('get tweets');
        const userNames = tweets.map(x => x.user.screen_name);
        // this.logger.debug('get usernames');

        for (let userName of userNames) {
            const user = (await this.getUserByUserName(userName).toPromise()).data;
            // this.logger.debug('get user');

            //following count control
            if(user.friends_count === 0){
                this.logger.debug(userName, 'failed friend count 0 control');
                continue;
            }

            const followerFriendRatio=parseFloat(user.followers_count) / parseFloat(user.friends_count);

            if(followerFriendRatio>1.5 || followerFriendRatio<0.6){
                this.logger.debug(userName, 'failed follower friend ratio control');
                continue;
            }

            //account create date control
            if (((new Date().getTime() - (new Date(user.created_at)).getTime()) / 1000.0 / 60.0 / 60.0 / 24.0) < 90) {
                this.logger.debug(userName, 'failed create date control');
                continue;
            }
            const userTweets = (await this.getTweetsByUserName(userName).toPromise()).data;
            // this.logger.debug('get tweets of user');

            //tweet count control
            if (userTweets.length < 20) {
                this.logger.debug(userName, 'failed tweet length control');
                continue;
            }

            //check if 20th tweet is older than 45 days
            const last = userTweets[userTweets.length - 1];
            if (((new Date().getTime() - (new Date(last.created_at)).getTime()) / 1000.0 / 60.0 / 60.0 / 24.0) > 45) {
                this.logger.debug(userName, 'failed last tweet older than 45 control');
                continue;
            }

            const followData = (await this.followUserByUserName(userName).toPromise()).data;
            this.logger.debug(userName + ' tried to follow ');

        }

    }


}
