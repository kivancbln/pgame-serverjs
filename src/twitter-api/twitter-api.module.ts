import { Module, HttpModule } from '@nestjs/common';
import { TwitterApiService } from './twitter-api.service';


@Module({
    imports: [HttpModule],
  providers: [TwitterApiService],
  exports: [TwitterApiService]
})
export class TwitterApiModule {}