import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TwitterApiModule } from './twitter-api/twitter-api.module';
import { ScheduleModule } from '@nestjs/schedule';
import { OddsModule } from './football-api/odds/odds.module';


@Module({
  imports: [TwitterApiModule,OddsModule, ScheduleModule.forRoot()],
  controllers: [AppController],
  providers: [AppService]
})
export class AppModule {
  constructor(){
    
  }
}
